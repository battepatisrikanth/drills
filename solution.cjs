const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


const availItems=items.filter((item)=>item['available'] == true);//1
const vitaminCItems=items.filter((item)=>item['contains']=='Vitamin C');//2
const vitaminAItems=items.filter((item)=>item['contains'].includes("Vitamin A"));//3

const sortItems=items.sort((a, b) => {
    return a['contains'].length - b['contains'].length;
});//5
// console.log(sortItems);

// function keyChange(){
//     const vitamins=items.reduce((acc,result) => {
//         const resultData=result.contains.split(', ');
//         resultData.reduce((acc2,item) =>{
//             if(acc[item]){
//                 acc[item].push(result.name);
//             }
//             else{
//                 acc[item]=[result.name];
//             }
//             return acc;
//         },{});

//     return acc; 

//     },{});   
    
// }

// console.log(vitamins);
    







