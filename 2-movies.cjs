const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}
// const favouritesMoviesKeys=Object.keys(favouritesMovies);
// const favouritesMoviesValues=Object.values(favouritesMovies);

// console.log(favouritesMoviesKeys);
// console.log(favouritesMoviesValues);

const favouritesMoviesArray=Object.entries(favouritesMovies);
// console.log(favouritesMoviesArray);

const moviesGreaterThanThreeHundred=favouritesMoviesArray.filter((element)=>{
    if(element[1].totalEarnings.substring(1,3)>'300'){
        return element;
    }
})
console.log(moviesGreaterThanThreeHundred);

const nominationsTotalEArningsAndFiveHundred=favouritesMoviesArray.filter((element)=>{
    if(element[1].oscarNominations>3 && (element[1].totalEarnings.substring(1,3))>'500'){
        return element;
    }
})

console.log(nominationsTotalEArningsAndFiveHundred);


const moviesOfActorDiCaprio=favouritesMoviesArray.filter((element)=>{
    if(element[1].actors.includes("Leonardo Dicaprio")){
        return element;
    }
})
console.log(moviesOfActorDiCaprio);

const sortMovies= favouritesMoviesArray.sort(function(firstItem,secondItem){
    if(firstItem[1].imdbRating == secondItem[1].imdbRating){
        return Number(firstItem[1].totalEarnings.slice(1,3))-Number(secondItem[1].totalEarnings.slice(1,3));
    }
    return firstItem[1].imdbRating-secondItem[1].imdbRating;
})

console.log(sortMovies);
const moviesGenres={"drama":5 , 
    "sci-fi":4 ,
    "adventure":3 ,
    "thriller":2 ,
    "crime":1};
const moviesWithOneGenre=favouritesMoviesArray.map((element)=>{
    if(element[1].genre.length>1){
    
        if(moviesGenres[element[1].genre[0]] > moviesGenres[element[1].genre[1]]){
            element[1].genre=element[1].genre[0]
        }else{
            element[1].genre = element[1].genre[1];
        }
        
    }
    return element;
})

// console.log(Array.isArray(moviesWithOneGenre));

// console.log(moviesWithOneGenre);
// const moviesGroup=moviesWithOneGenre.groupBy((element)=>{
//     return element[1].genre;
// });
// console.log(moviesGroup);

function groupBy(objectArray, property) {
    return objectArray.reduce((acc, obj) => {
       const key = obj[1][property];
       if (!acc[key]) {
          acc[key] = [];
       }
       // Add object to list for given key's value
       acc[key].push(obj);
       return acc;
    }, {});
 }
const groupedMovies = groupBy(moviesWithOneGenre,"genre");
console.log(groupedMovies);





/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 
