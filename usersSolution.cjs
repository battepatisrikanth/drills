const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


let objectArray=Object.entries(users);
// console.log(objectArray);
// console.log(objectArray[0][1]['interests']);

 const germanyValues=objectArray.filter((item)=>{
     if(item[1]["nationality"]== "Germany"){         //2
         return item[0];
     }
 })

console.log(germanyValues);


const videoGameValues=objectArray.filter((item)=>{
    if(String(item[1]["interests"]).includes("Video Games") ){ //1
        return item[0];
    }
})

console.log(videoGameValues);



const mastersValues=objectArray.filter((item)=>{
    if(String(item[1]["qualification"]).includes("Masters")){//4
        return item[0];
    }
})

console.log(mastersValues);




const designationObject={'Senior Developer':1, //3
    'Developer':2,'Intern':3
};

// const toSortArray=objectArray.map(callback);






const sortValues= objectArray.sort((firstItem,secondItem)=>{
    if(String(firstItem[1].desgination).includes("senior") && String(secondItem[1].desgination).includes("senior")){
    return secondItem[1].age-firstItem[1].age;
    }
    else if(String(firstItem[1].desgination).includes("senior") && !(String(secondItem[1].desgination).substring("senior"))){
    return firstItem ;
    }
})

// console.log(sortValues);

const groupUsers = objectArray.group(({ type }) => type);//5

// console.log(groupUsers);